import '/imports/startup/client';
import { TAPi18n } from 'meteor/tap:i18n';
import { sAlert } from 'meteor/juliancwirko:s-alert';

Meteor.startup(function () {
  TAPi18n.setLanguage('ru');
  sAlert.config({ position: 'bottom-left', timeout: 3000, effect: 'slide' });
});
