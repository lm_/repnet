import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.methods({
  "accounts/add": function(user, email, password){
    check(user, String);
    check(email, String);
    check(password, String);
    
    try {
      Accounts.createUser({ username: user, password: password, email: email });
    } catch(err) {
      return err;
    }
    return true;
  },
  "accounts/setPassword": function(userId, password){
    check(userId, String);
    check(password, String);
    try {
      Accounts.setPassword(userId, password);
    } catch(error) {
      return error; 
    }
    return true;
  },
  "accounts/update": function (userId, username, email) {
    check(userId, String);
    check(username, String);
    check(email, String);
    let oldmail = 'example@example.com';
    let user = Meteor.users.findOne(userId);
    if (user.emails) {
      if (user.emails.length > 0) {
        oldmail = user.emails[0].address;
      }
    }
    try {
      Accounts.setUsername(userId, username);
      Accounts.removeEmail(userId, oldmail);
      Accounts.addEmail(userId, email);
    } catch(err) {
      return err;
    }
    return true;
  },
  "accounts/remove": function (userId) {
    check(userId, String);
    try {
      Meteor.users.remove(userId);
    } catch(error) {
      return error;
    }
    return true;
  }
});

