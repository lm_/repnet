﻿import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Brands = new Mongo.Collection('Brands');

Brands.schema = new SimpleSchema({
  name: { type: String },
  userId: { type: String },
  lang: { type: String },
/*  exceptions: {
    type: [String],
    defaultValue: [], 
    regEx: [
      SimpleSchema.RegEx.Url,
      SimpleSchema.RegEx.Domain,
      SimpleSchema.RegEx.WeakDomain
    ]
  }, */
  enabled: { type: Boolean, defaultValue: true },
  updateInterval: { type: String, defaultValue: 'at 6:00 am' },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();
      }
    }
  },
/*  updatedAt: {
    type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
  }, */
  updatedAt: { type: Date },
/*
  history: {
    type: Array,
    optional: true,
  },
  'history.$': {
    type: Object,
  },
  'history.$.timestamp':{
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();
      }
    }
  },
  'history.$.sources':{
    type: Array,
    optional: true,
  },
  'history.$.sources.$': {
    type: Object,
  },
  'history.$.sources.$.url': {
    type: String,
    regEx: [
      SimpleSchema.RegEx.Url,
      SimpleSchema.RegEx.Domain,
      SimpleSchema.RegEx.WeakDomain
    ],
  },
  'history.$.sources.$.value': {
    type: Number,
  } */
});

Brands.attachSchema(Brands.schema);

