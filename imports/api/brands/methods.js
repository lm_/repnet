﻿import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Brands } from './brands.js';

Meteor.methods({
  "brands/add": function(userId, name, lang, enabled) {
    check(userId, String);
    check(name, String);
    check(lang, String);
//    check(exceptions, Match.Maybe([String]));
    check(enabled, Boolean);
    try {
      let brandId = Brands.insert({
        name: name,
        userId: userId,
        lang: lang,
//        exceptions: exceptions,
        enabled: enabled,
        updatedAt: new Date()
      });
      Meteor.call("tweets/search", brandId, name, lang, function(err, result) {
        if (err) {
          console.log(err);
        }
        if (result) {
          Brands.update({ _id: brandId }, {
            $set: {
              updatedAt: new Date(),
            }
          });
        }
      });
      return true;
    } catch(err) {
      return err;
    }
  },
  "brands/remove": function (brandId) {
    check(brandId, String);
    try {
      Brands.remove(brandId);
    } catch(error) {
      return error;
    }
    return true;
  },
  "brands/edit": function(brandId, enabled) {
    check(brandId, String);
//    check(exceptions, Match.Maybe([String]));
    check(enabled, Boolean);

    try {
      Brands.update({ _id: brandId }, {
        $set: {
//          exceptions: exceptions,
          enabled: enabled
        }
      });
    } catch(err) {
      return err;
    }
    return true;
  },
  "brands/update": function(brandId) {
    check(brandId, String);

    let brand = Brands.findOne(brandId);
    if (brand) {
      Meteor.call("tweets/search", brand._id, brand.name, brand.lang, function(err, result) {
        if (err) {
          console.log(err);
        }
        if (result) {
          Brands.update({ _id: brand._id }, {
            $set: {
              updatedAt: new Date(),
            }
          });
        }
      });
    }
  }
});

