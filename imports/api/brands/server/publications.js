﻿import { Meteor } from 'meteor/meteor';
import { Brands } from '../brands.js';

Meteor.publish("Brands_list", function(userId){
  return Brands.find({ userId: userId }, { fields: { name: 1 } });
});

Meteor.publish("Brand", function(brandId) {
  return Brands.find({ _id: brandId });
});

