import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Stems } from '../stems/stems.js';

Meteor.methods({
  "languages/import": function(text, lang){
    check(text, Array);
    check(lang, String);

    let count = 0;
    for (let i = 0; i < text.length - 1; i++) {
      if (Stems.find({ body: text[i] }).count() < 1) {
//        console.log(text[i] + " " + i + "/" + text.length);
        Stems.insert({ body: text[i], lang: lang });
        count = count + 1;
      }
      console.log(i + "/" + text.length);
    }
    return count;
  },
  "languages/clear": function(lang) {
    check(lang, String);

    try {
      Stems.remove({ lang: lang });
      return true;
    } catch(err) {
      return err;
    }
  }
});

