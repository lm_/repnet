﻿import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Settings } from './settings.js';

Meteor.methods({
  "settings/update": function(consumer_key, consumer_secret, access_token, access_secret){
    check(consumer_key, String);
    check(consumer_secret, String);
    check(access_token, String);
    check(access_secret, String);

    try {
      let settingId = Settings.findOne()._id;
      Settings.update(settingId, {
        $set: {
          consumer_key: consumer_key,
          consumer_secret: consumer_secret,
          access_token: access_token,
          access_secret: access_secret
        }
      });
      return true;
    } catch (err) {
      return err;
    }
  }
});

