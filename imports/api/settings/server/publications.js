﻿import { Meteor } from 'meteor/meteor';
import { Settings } from '../settings.js';

Meteor.publish("Settings", function(){
  return Settings.find();
});

