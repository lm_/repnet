﻿import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Settings = new Mongo.Collection('Settings');

Settings.schema = new SimpleSchema({
  consumer_key: { type: String, optional: true },
  consumer_secret: { type: String, optional: true },
  access_token: { type: String, optional: true },
  access_secret: { type: String, optional: true },
/*  networks: { type: Array, optional: true },
  'networks.$': { type: Object },
  'networks.$.lang': { type: String },
  'networks.$.body': { type: String }, */
  network: { type: Object, optional: true },
  'network.body': { type: String, optional: true },
  'network.status': { type: String, optional: true },
  'network.error': { type: Number, optional: true, decimal: true },
  'network.iterations': { type: Number, optional: true },
  'network.maxlen': { type: Number, optional: true }
});

Settings.attachSchema(Settings.schema);



