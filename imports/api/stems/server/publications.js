﻿import { Meteor } from 'meteor/meteor';
import { Stems } from '../stems.js';

Meteor.publish("Stems", function(){
  return Stems.find();
});

