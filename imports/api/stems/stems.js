﻿import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Stems = new Mongo.Collection('Stems');

Stems.schema = new SimpleSchema({
  body: { type: String },
  lang: { type: String },
  index: {
    type: Number,
    decimal: true,
    autoValue: function() {
      if (this.isInsert) {
/*        let index = Stems.find().count() + 1;
        let count = Math.floor(Math.log(index) / Math.log(10)) + 1;
        for (let i = 0; i < count; i++) {
          index = index/10;
        }
        let d = parseFloat(index.toFixed(count));
        return d; */
        return Stems.find().count() + 1;
      }
    }
  },
});

Stems.attachSchema(Stems.schema);

