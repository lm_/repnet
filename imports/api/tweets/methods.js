import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Tweets } from './tweets.js';
import { Settings } from '../settings/settings.js';

Meteor.methods({
  'tweets/search': function(brandId, term, lang) {
    check(brandId, String);
    check(term, String);
    check(lang, String);

    let settings = Settings.findOne();
    let Twit = new TwitMaker({
      consumer_key: settings.consumer_key,
      consumer_secret: settings.consumer_secret,
      access_token: settings.access_token,
      access_token_secret: settings.access_secret
    });
    
    Twit.get('search/tweets', { q: term, count: 100, lang: lang }, Meteor.bindEnvironment(function(err, data, response) {
      if(data) {
        if(data.search_metadata.count>0) {
          for (let i = 0; i < data.statuses.length; i++) {
            Meteor.call("synaptic/test", data.statuses[i].text, function(error, result) {
              if (result) {
                Tweets.update({ tweetId: data.statuses[i].id }, {
                  $set: {
                    brandId: brandId,
                    tweetId: data.statuses[i].id,
                    disabled: false,
                    text: data.statuses[i].text,
                    date: new Date(data.statuses[i].created_at.replace(/^\w+ (\w+) (\d+) ([\d:]+) \+0000 (\d+)$/,"$1 $2 $4 $3 UTC")),
                    url: data.statuses[i].entities.urls[0].url,
                    value: (result[0]*100).toFixed(),
                  },
                }, { upsert: true }, function(err2, res2) {
                  if(err2) {
                    console.log(err2);
                  }
                });
              }
            });
          }
        } 
      }
    }));
  },
  'tweets/edit': function(tweetId, disabled, value) {
    check(tweetId, String);
    check(disabled, Boolean);
    check(value, Number);

    try {
      Tweets.update({ _id: tweetId }, {
        $set: {
          disabled: disabled,
          value: value
        }
      });
      return true;
    } catch(err) {
      return err;
    }
  }
});

