import { Meteor } from 'meteor/meteor';
import { FindFromPublication } from 'meteor/percolate:find-from-publication';
import { Tweets } from '../tweets.js';

Meteor.publish("Tweets", function(brandId, disabled) {
  return Tweets.find({ brandId: brandId });
});

FindFromPublication.publish("Tweets.filtered", function(brandId, disabled, filtered) {
  if (filtered) {
    return Tweets.find({ brandId: brandId, disabled: disabled }, { limit: 10, sort: { date: -1 } });
  } else {
    return Tweets.find({ brandId: brandId, disabled: disabled }, { sort: { date: -1 } });
  }
});

