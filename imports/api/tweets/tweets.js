import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Tweets = new Mongo.Collection('Tweets');

Tweets.schema = new SimpleSchema({
  brandId: { type: String },
  tweetId: { type: Number },
  disabled: { type: Boolean },
  text: { type: String, optional: true },
  date: { type: Date, optional: true },
  url: { type: String, optional: true },
  value: { type: Number, decimal: true }
});

Tweets.attachSchema(Tweets.schema);

