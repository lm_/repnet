import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Session } from 'meteor/session';

import '../../ui/components/nav.js';
import '../../ui/components/footer.js';
import '../../ui/layouts/front.js';
import '../../ui/layouts/body.js';
import '../../ui/components/brand-add-form.js';
import '../../ui/components/brand-remove-form.js';
import '../../ui/components/brand-edit-form.js';
import '../../ui/components/brands-list.js';
import '../../ui/pages/brands-page.js';
import '../../ui/pages/login-page.js';
import '../../ui/pages/register-page.js';
import '../../ui/pages/not-found-page.js';
import '../../ui/components/account-edit-form.js';
import '../../ui/components/account-remove-form.js';
import '../../ui/components/account-set-password-form.js';
import '../../ui/components/account-add-form.js';
import '../../ui/components/accounts-list.js';
import '../../ui/components/language-clear-form.js';
import '../../ui/components/languages-list.js';
import '../../ui/components/settings-list.js';
import '../../ui/components/nnet-clear-form.js';
import '../../ui/components/nnet-settings.js';
import '../../ui/pages/manage-page.js';
import '../../ui/pages/test-page.js';
import '../../ui/components/chart-pie.js';
import '../../ui/components/chart-line.js';
import '../../ui/components/summary-widget.js';
import '../../ui/components/tweets-list-item.js';
import '../../ui/components/tweets-list.js';
import '../../ui/components/tweet-edit-form.js';
import '../../ui/pages/brand-page.js';

FlowRouter.wait();
Tracker.autorun(function(){
  if (Roles.subscription.ready()&&!FlowRouter._initialized){
    FlowRouter.initialize();
  }
});

exposed = FlowRouter.group({
  name: 'App.public'
});

exposed.route('/', {
  name: 'App.public.home',
  action() {
    BlazeLayout.render('App_front');
  },
});

exposed.route('/test', {
  name: 'App.public.test',
  action() {
    BlazeLayout.render('App_body', { main: 'Test_page' });
  },
});

exposed.route('/login', {
  name: 'App.public.login',
  action() {
    BlazeLayout.render('App_body', { main: 'Login_page' });
  },
});

exposed.route('/register', {
  name: 'App.public.register',
  action() {
    BlazeLayout.render('App_body', { main: 'Register_page' });
  },
});

protected = FlowRouter.group({
  name: 'App.private',
  prefix: '/app',
  triggersEnter: [
    function() {
      if (!(Meteor.loggingIn() || Meteor.userId())) {
        let route = FlowRouter.current();
        if (route.route.name !== 'App.public.login') {
          Session.set('redirectAfterLogin', route.path);
        }
        return FlowRouter.go('App.public.login');
      }
    }, 
  ],
});

protected.route('/', {
  name: 'App.private.root',
  action() {
    BlazeLayout.render('App_body', { main: 'Brands_page' });
  }
});

protected.route('/brands/:_id', {
  name: 'App.private.brand',
  action() {
    BlazeLayout.render('App_body', { main: 'Brand_page' });
  }
});

restricted = protected.group({
  prefix: '/manage',
  triggersEnter: [
    function() {
      if (Meteor.userId()) {
        if (!Roles.userIsInRole(Meteor.user(), 'admin')) {
          return FlowRouter.go('App.public.home');
        }
      }
    }
  ],
});

restricted.route('/', {
  name: 'App.private.manage',
  action() {
    BlazeLayout.render('App_body', { main: 'Manage_page' });
  }
});

FlowRouter.notFound = {
  action() {
    BlazeLayout.render('App_body', { main: 'Not_found_page' });
  }
}

