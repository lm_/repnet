import { Meteor } from 'meteor/meteor';
import { Settings } from '../../api/settings/settings.js';

Meteor.startup(function() {
   
  if (Meteor.users.find().count() < 1) {
    userId = Accounts.createUser({
      username: "admin",
      password: "repnet123"
    });
    Roles.addUsersToRoles(userId, 'admin', Roles.GLOBAL_GROUP);
  }

  if (Settings.find().count() < 1) {
    Settings.insert({
      gmail_user: 'example@google.com',
      gmail_password: '123',
      network: {
        status: 'nnet.status.not-initialized',
        error: 0,
        iterations: 0,
        maxlen: 0
      }
    });
  }
});

