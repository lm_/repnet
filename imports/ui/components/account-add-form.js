import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './account-add-form.html';

Template.Account_add_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let user = template.$('input[id="account-add-username"]').val();
    let email = template.$('input[id="account-add-email"]').val();
    if (user&&email) {
      Meteor.call("accounts/add", user, email, user + '123', function(err, result) {
        if (err) {
          sAlert.error(err.reason);
       }
       if (result) {
         sAlert.success(TAPi18n.__('admin.alerts.account-created'));
         Overlay.hide();
       }
      });
    } else {
      sAlert.error(TAPi18n.__('accounts.alerts.credentials'));
    }
  }
});

