import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './account-edit-form.html';

Template.Account_edit_form.helpers({
  'account': function() {
    return Meteor.users.findOne(Session.get('accounts-list-selected'));
  },
  'email': function() {
    if (this.emails) {
      return this.emails[0].address;
    }
  }
});


Template.Account_edit_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let user = template.$('input[id="account-edit-username"]').val();
    let email = template.$('input[id="account-edit-email"]').val();
    if (user&&email) {
      Meteor.call("accounts/update", Session.get('accounts-list-selected'), user, email, function(err, result) {
        if (err) {
          sAlert.error(err.reason);
       }
       if (result) {
         sAlert.success(TAPi18n.__('admin.alerts.account-edited'));
         Overlay.hide();
       }
      });
    } else {
      sAlert.error(TAPi18n.__('accounts.alerts.credentials'));
    }
  }
});

