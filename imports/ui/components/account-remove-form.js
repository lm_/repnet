import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './account-remove-form.html';

Template.Account_remove_form.helpers({
  'account': function() {
    return Meteor.users.findOne(Session.get('accounts-list-selected'));
  },
});

Template.Account_remove_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    Meteor.call("accounts/remove", (Session.get('accounts-list-selected')), function(err, res) {
      if(err){
        sAlert.error(err.reason);
      }
      if (res) {
        sAlert.success(TAPi18n.__('admin.alerts.account-removed'));
        Overlay.hide();
      }
    });
  }
});

