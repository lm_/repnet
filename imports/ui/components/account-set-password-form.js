import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './account-set-password-form.html';

Template.Account_set_password_form.helpers({
  'account': function() {
    return Meteor.users.findOne(Session.get('accounts-list-selected'));
  },
  'email': function() {
    if (this.emails) {
      return this.emails[0].address;
    }
  }
});


Template.Account_set_password_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let newpass = template.$('input[id="account-set-password"]').val();
    if (newpass) {
      Meteor.call("accounts/setPassword", Session.get('accounts-list-selected'), newpass, function(err, result) {
        if (err) {
          sAlert.error(err.reason);
       }
       if (result) {
         sAlert.success(TAPi18n.__('admin.alerts.account-edited'));
         Overlay.hide();
       }
      });
    } else {
      sAlert.error(TAPi18n.__('accounts.alerts.credentials'));
    }
  }
});

