import { Template } from 'meteor/templating';

import './accounts-list.html';

Template.Accounts_list.onCreated(function() {
  this.subscribe("Accounts");
});

Template.Accounts_list.helpers({
  accounts: function() {
    return Meteor.users.find();
  },
  email: function(account) {
    if (account.emails) {
      return account.emails[0].address;
    }
  },
});

Template.Accounts_list.events({
  'click [data-event-action=account-edit]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    
    Session.set('accounts-list-selected', event.currentTarget.id);
    Overlay.show('Account_edit_form');
  },
  'click [data-event-action=account-add]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Overlay.show('Account_add_form');
  },
  'click [data-event-action=account-remove]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    Session.set('accounts-list-selected', event.currentTarget.id);
    Overlay.show('Account_remove_form');
  },
  'click [data-event-action=account-set-password]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    Session.set('accounts-list-selected', event.currentTarget.id);
    Overlay.show('Account_set_password_form');
  },
  'mouseenter .list-row': function(e, t){
    e.preventDefault();
    e.stopPropagation();

    if (e.target.cells) {
//      event.target.cells[2].style.display = 'block';
      e.target.cells[2].childNodes[1].style.display = 'block';
    }
//      t.$('.list-cell').show();
  },
  'mouseleave .list-row': function(e, t) {
    e.preventDefault();
    e.stopPropagation();

    if (e.target.cells) {
//      event.target.cells[2].style.display = 'none';
      e.target.cells[2].childNodes[1].style.display = 'none';
    }
//      t.$('.list-cell').hide();
  }
});

