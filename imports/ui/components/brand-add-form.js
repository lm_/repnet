import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './brand-add-form.html';

Template.Brand_add_form.helpers({
  'languages': function() {
    let langs = TAPi18n.getLanguages();
    let l = [];
    for(let lang in langs) {
      l.push({ code: lang, name: langs[lang].name, en: langs[lang].en });
    }
    return l;
  }
});

Template.Brand_add_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let name = template.$('input[id="brand-add-name"]').val();
    let lang = template.$('select[id="brand-add-language"]').val();
//    let exceptions = template.$('textarea[id="brand-add-exceptions"]').val().split('\n');
    let enabled = template.$('input[id="brand-add-enabled"]').is(':checked');
    if (name&&lang) {
      Meteor.call("brands/add", Meteor.userId(), name, lang, enabled, function(err, result) {
        if (err) {
          sAlert.error(err.reason);
       }
       if (result) {
         sAlert.success(TAPi18n.__('brands.alerts.brand-added'));
         Overlay.hide();
//         Meteor.call("tweets/search", name, lang);
       }
      });
    } else {
      sAlert.error(TAPi18n.__('accounts.alerts.credentials'));
    }
/*    console.log(name);
    console.log(lang);
    console.log(exceptions);
    console.log(enabled); */
  }
});

