import { Template } from 'meteor/templating';
import { Brands } from '../../api/brands/brands.js';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './brand-edit-form.html';

Template.Brand_edit_form.helpers({
  'brand': function() {
    let brand = Brands.findOne(Session.get('brands-list-selected'));
    console.log(brand);
    return brand;
  },
  'exceptions': function() {
    let exceptions = "";
    let exc_arr = Brands.findOne(Session.get('brands-list-selected')).exceptions;
    for(let i = 0; i < exc_arr.length; i++) {
      exceptions = exceptions + exc_arr[i];
      if (i < (exc_arr.length - 1)) {
        exceptions = exceptions + '\n';
      }
    }
    if (exceptions.len > 0) {
      return exceptions;
    }
  },
/*  'enabled': function() {
    return Brands.findOne(Session.get('brands-list-selected')).enabled;
  } */
  'language': function(lang) {
    let langs = TAPi18n.getLanguages();
    if (langs[lang]) {
      return langs[lang].name;
    }
  }
});

Template.Brand_edit_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
//    let exceptions = template.$('textarea[id="brand-edit-exceptions"]').val().split('\n');
    let enabled = template.$('input[id="brand-edit-enabled"]').is(':checked');
    Meteor.call("brands/edit", Session.get('brands-list-selected'), enabled, function(err, result) {
      if (err) {
        sAlert.error(err.reason);
      }
      if (result) {
        sAlert.success(TAPi18n.__('brands.alerts.brand-updated'));
        Overlay.hide();
      }
    });
  }
});

