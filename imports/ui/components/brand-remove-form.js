import { Template } from 'meteor/templating';
import { Brands } from '../../api/brands/brands.js';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './brand-remove-form.html';

Template.Brand_remove_form.helpers({
  'brand': function() {
    return Brands.findOne(Session.get('brands-list-selected'));
  },
});

Template.Brand_remove_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    Meteor.call("brands/remove", (Session.get('brands-list-selected')), function(err, res) {
      if(err){
        sAlert.error(err.reason);
      }
      if (res) {
        sAlert.success(TAPi18n.__('brands.alerts.brand-removed'));
        Overlay.hide();
      }
    });
  }
});

