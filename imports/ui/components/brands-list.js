import { Template } from 'meteor/templating';
import { Brands } from '../../api/brands/brands.js';

import './brands-list.html';

Template.Brands_list.onCreated(function() {
  this.subscribe("Brands_list", Meteor.userId());
});

Template.Brands_list.helpers({
  brands: function() {
    return Brands.find();
  },
});

Template.Brands_list.events({
  'click [data-event-action=brand-edit]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    
    Session.set('brands-list-selected', event.currentTarget.id);
    Overlay.show('Brand_edit_form');
  },
  'click [data-event-action=brand-add]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Overlay.show('Brand_add_form');
  },
  'click [data-event-action=brand-remove]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    Session.set('brands-list-selected', event.currentTarget.id);
    Overlay.show('Brand_remove_form');
  },
  'mouseenter .list-row': function(e, t){
    e.preventDefault();
    e.stopPropagation();

    if (e.target.cells) {
//      event.target.cells[2].style.display = 'block';
      e.target.cells[1].childNodes[1].style.display = 'block';
    }
  },
  'mouseleave .list-row': function(e, t) {
    e.preventDefault();
    e.stopPropagation();

    if (e.target.cells) {
//      e.target.cells[2].style.display = 'none';
      e.target.cells[1].childNodes[1].style.display = 'none';
    }
  }
});

