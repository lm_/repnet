import { Template } from 'meteor/templating';
import { Chart } from 'meteor/chart:chart';

import './chart-line.html';

let random = function () {
  return Math.floor((Math.random() * 100) + 1);
}

Template.Chart_line.onRendered(function(){
  let context = document.getElementById("chart_line").getContext("2d");

  let date = new Date();
  date.setHours(0,0,0,0);
  let dates = [];
  for (let i = 6; i > -1; i--) {
    let tmp = new Date(date);
    tmp = new Date(tmp.setDate(tmp.getDate() - i));
    dates.push(tmp);
  }

  let data = {};
  data.labels = [];
  for (let i = 0; i < dates.length; i++) {
    data.labels.push(dates[i].toLocaleDateString('ru', { weekday: 'short', day: 'numeric', month: 'numeric' }));
  }
  data.datasets = []
  data.datasets[0] = {
    label: TAPi18n.__('brands.mentions'),
    fillColor: "rgba(77,136,255,0)",
    strokeColor: "rgba(77,136,255,1)",
    pointColor: "rgba(77,136,255,1)",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "rgba(77,136,255,1)",
    data: this.data.mentions
  }
  
  data.datasets[1] = {
    label: TAPi18n.__('brands.positive'),
    fillColor: "rgba(49,180,49,0)",
    strokeColor: "rgba(49,180,49,1)",
    pointColor: "rgba(49,180,49,1)",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "rgba(49,180,49,1)",
    data: this.data.positive
  }

  data.datasets[2] = {
    label: TAPi18n.__('brands.negative'),
    fillColor: "rgba(220,220,220,0)",
    strokeColor: "rgba(215,40,40,1)",
    pointColor: "rgba(215,40,40,1)",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "rgba(215,40,40,1)",
    data: this.data.negative
  }
  let ChartLine = new Chart(context).Line(data, {
    responsive: true, 
    maintainAspectRatio: false,
    multiTooltipTemplate: '<%= datasetLabel %>: <%= value %>',
    legend: {
      display: true,
      position: 'right'
    }
  });
});

