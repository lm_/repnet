import { Template } from 'meteor/templating';
import { Chart } from 'meteor/chart:chart';

import './chart-pie.html';

Template.Chart_pie.onRendered(function(){
  let context = document.getElementById("chart_pie").getContext("2d");
//  let val = this.data[0] * 100;
//  let pos = val.toFixed();
//  let neg = 100 - pos;
  let data = [
    { value: this.data[0], color: "#31b431", highlight: "#4bce4b", label: TAPi18n.__('brands.positive')},
    { value: this.data[1], color: "#d72828", highlight: "#df5353", label: TAPi18n.__('brands.negative')},
  ]
  let ChartPie = new Chart(context).Doughnut(data, { responsive: true, maintainAspectRatio: false });
});

