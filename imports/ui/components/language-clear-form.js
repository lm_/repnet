import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './language-clear-form.html';

Template.Language_clear_form.helpers({
  'language': function() {
    return TAPi18n.getLanguages()[Session.get('languages-list-selected')];
  }
});

Template.Language_clear_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    Meteor.call("languages/clear", (Session.get('languages-list-selected')), function(err, res) {
      if (err) {
        sAlert.error(err.reason);
      }
      if (res) {
        sAlert.success(TAPi18n.__('admin.alerts.stems-cleared'));
        Overlay.hide();
      }
    });
  }
});

