import { Template } from 'meteor/templating';

import './nav.html';

Template.Nav.helpers({
  'home': function() {
    if (FlowRouter.getRouteName() == 'App.public.home') {
      return true;
    }
    return false;
  },
  'test': function() {
    if (FlowRouter.getRouteName() == 'App.public.test') {
      return true;
    }
    return false;
  },
  'manage': function() {
    if (FlowRouter.getRouteName() == 'App.private.manage') {
      return true;
    }
    return false;
  },
  'app': function() {
    let path = FlowRouter.getRouteName();
    if(path) {
      path = path.split('.');
      if (path[0] + '.' + path[1] == 'App.private') {
        if (path[2]) {
          if (path[2] != 'manage') {
            return true;
          }
        } else {
          return true;
        }
      }
    }
    return false;
  },
  'loginregister': function() {
    if (FlowRouter.getRouteName() == 'App.public.login' || FlowRouter.getRouteName() == 'App.public.register') {
      return true;
    }
    return false;
  },
});

Template.Nav.events({
  'click [data-event-action=nav-logout]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Meteor.logout(() => {
      FlowRouter.go("App.public.home");
    });
  },
});


