import { Template } from 'meteor/templating';
import { Settings } from '../../api/settings/settings.js';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './nnet-clear-form.html';

Template.NNet_clear_form.onCreated(function() {
  this.subscribe("Settings");
});

Template.NNet_clear_form.helpers({
  'body': function() {
    return Settings.findOne().network.body;
  }
});

Template.NNet_clear_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

/*    Meteor.call("brain/clear", function(err, res) {
      if (err) {
        sAlert.error(err.reason);
      }
      if (res) {
        sAlert.success(TAPi18n.__('nnet.alerts.brain-cleared'));
        Overlay.hide();
      }
    }); */

    let nnet = template.$('textarea[id="nnet-clear-body"]').val();
    if (nnet) {
      Meteor.call("synaptic/import", nnet, function(err, res) {
        if (err) {
          sAlert.error(err.reason);
        }
        if (res) {
          sAlert.success(TAPi18n.__('nnet.alerts.nnet-imported'));
          Overlay.hide();
        }
      });
    } else {
       sAlert.error(TAPi18n.__('nnet.alerts.no-input'));
    }
  }
});

