import { Template } from 'meteor/templating';
import { Settings } from '../../api/settings/settings.js';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './nnet-settings.html';

Template.NNet_settings.onCreated(function() {
  this.subscribe("Settings");
});

Template.NNet_settings.helpers({
  'network': function() {
    return Settings.findOne().network;
  },
  'status': function(){
    return TAPi18n.__(this.status);
  }
});

Template.NNet_settings.events({
  'click [data-event-action=nnet-settings-train]': function(e, t) {
    e.stopPropagation();
    e.preventDefault();

    let input = document.getElementById('nnet-settings-input');
    if (input.files[0]) {
      let file = input.files[0];
      let reader = new FileReader();
      reader.onload = function(evt) {
        if (evt.target.result.length < 1) {
          sAlert.error(TAPi18n.__('nnet.alerts.no-input'));
        } else {
          Meteor.call('synaptic/train', evt.target.result.replace(/\s+/g," "), function(err, res) {
            if (err) {
              sAlert.error(err.reason);
            }
            if (res) {
              sAlert.success(TAPi18n.__('nnet.alerts.nnet-trained'));
            }
          });
        }
      }
      reader.readAsText(file);
    } else {
      sAlert.error(TAPi18n.__('nnet.alerts.train-no-file'));
    }
  },
  'click [data-event-action=nnet-settings-clear]': function(e, t) {
    e.stopPropagation();
    e.preventDefault();
    Overlay.show('NNet_clear_form');
  }
});

