import { Template } from 'meteor/templating';
import { Settings } from '../../api/settings/settings.js';

import './settings-list.html';

Template.Settings_list.onCreated(function() {
  this.subscribe("Settings");
});

Template.Settings_list.helpers({
  settings: function() {
    return Settings.findOne();
  }
});

Template.Settings_list.events({
  'click [data-event-action=settings-save]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    
    let consumer_key = template.$('input[id="settings-consumer-key"]').val();
    let consumer_secret = template.$('input[id="settings-consumer-secret"]').val();
    let access_token = template.$('input[id="settings-access-token"]').val();
    let access_secret = template.$('input[id="settings-access-secret"]').val();
    if(consumer_key&&consumer_secret&&access_token&&access_secret) {
      Meteor.call('settings/update', consumer_key, consumer_secret, access_token, access_secret, function(err, res) {
        if (err) {
          sAlert.error(err.reason);
        }
        if (res) {
          sAlert.success(TAPi18n.__('admin.alerts.settings-saved'));
        }
      });
    } else {
      sAlert.error(TAPi18n.__('accounts.alerts.credentials'));
    }
  }
});

