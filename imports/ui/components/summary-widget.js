import { Template } from 'meteor/templating';
import { Tweets } from '../../api/tweets/tweets.js';

import './summary-widget.html';

var view;

Template.Summary_widget.onCreated(function() {
  let self = this; 
  self.sub = Meteor.subscribe("Tweets", this.data._id);
});

let findAvg = function() {
  let tweets = Tweets.find({ disabled: false }).fetch();
  let avg = 0;
  for (let i = 0; i < tweets.length; i++) {
    avg = avg + tweets[i].value;
  }
  avg = avg / tweets.length;
  return avg;
}

Template.Summary_widget.onRendered(function() {
  let self = this;
  self.autorun(function() {
    if (self.sub.ready()) {
      if (view) {
        Blaze.remove(view);
      }
      let avg = findAvg();
      view = Blaze.renderWithData(Template.Chart_pie, [ avg, 100 - avg ], $("#summary-graph")[0]);
    }
  });
});



Template.Summary_widget.onDestroyed(function(){
  let self = this;
  self.sub.stop();
});

Template.Summary_widget.helpers({
  count: function() {
    let c = false;
    if (Tweets.find().count() > 0) {
      c = true;
    }
    return c;
  },
  positive: function(r) {
    if (r >= 50) {
      return true;
    }
    return false;
  },
  reputation: function() {
    return findAvg().toFixed();
  },
  mentions: function() {
    return Tweets.find({ disabled: false }).count();
  }
});

Template.Summary_widget.events({
  'change [id=summary-widget-mode]': function(e, t) {
    e.stopPropagation();
    e.preventDefault();

//    Session.set("Summary_widget_mode", t.$('select[id="summary-widget-mode"]').val());
    Blaze.remove(view);
    let mode = t.$('select[id="summary-widget-mode"]').val();
    if (mode == 0) {
      let avg = findAvg();
      view = Blaze.renderWithData(Template.Chart_pie, [ avg, 100 - avg ], $("#summary-graph")[0]);
    } else if (mode == 1) {
      let tweets = Tweets.find({ disabled: false }).fetch();
      let positive = 0;
      for (let i = 0; i < tweets.length; i++) {
        if (tweets[i].value >= 0.5) {
          positive = positive + 1;
        }
      }
      view = Blaze.renderWithData(Template.Chart_pie, [ positive, tweets.length - positive ], $("#summary-graph")[0]);
    } else if (mode == 2) {
      let end = new Date();
      let start = new Date(end);
      start = new Date(start.setDate(start.getDate() - 6));
      
      let data = {};
      data.mentions = [];
      data.positive = [];
      data.negative = [];

      for(let i = 0; i < 7; i++) {
        let tmp = new Date(start);
        tmp = new Date(tmp.setDate(tmp.getDate() + i));
        data.mentions.push(Tweets.find({ disabled: false, date: { $lte: tmp } }).count());
        data.positive.push(Tweets.find({ disabled: false, date: { $lte: tmp }, value: { $gte: 50 } }).count());
        data.negative.push(Tweets.find({ disabled: false, date: { $lte: tmp }, value: { $lt: 50 } }).count());
      }

      view = Blaze.renderWithData(Template.Chart_line, data, $("#summary-graph")[0]);
    }
  },
});

