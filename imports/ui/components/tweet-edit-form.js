import { Template } from 'meteor/templating';
import { Tweets } from '../../api/tweets/tweets.js';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './tweet-edit-form.html';

Template.Tweet_edit_form.helpers({
  'tweet': function() {
    return Tweets.findOne(Session.get('tweets-list-selected'));
  },
  'date': function() {
    return this.date.toLocaleDateString('ru', { timeZone: 'Europe/Moscow', weekdsy: 'long', day: 'numeric', month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric' });
  }
});

Template.Tweet_edit_form.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let disabled = template.$('input[id="tweet-edit-disable"]').is(':checked');
    let value = template.$('input[id="tweet-edit-value"]').val();
    Meteor.call("tweets/edit", Session.get('tweets-list-selected'), disabled, parseInt(value, 10), function(err, result) {
      if (err) {
        sAlert.error(err.reason);
      }
      if (result) {
        sAlert.success(TAPi18n.__('tweets.alerts.tweet-edited'));
        Overlay.hide();
      }
    });
  }
});

