import { Template } from 'meteor/templating';

import './tweets-list-item.html';

Template.Tweets_list_item.helpers({
  date: function() {
    return this.date.toLocaleDateString('ru', { timeZone: 'Europe/Moscow', day: 'numeric', month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric' });
  },
  positive: function() {
    if (this.value >= 50) {
      return true;
    }
    return false;
  }
});

Template.Tweets_list_item.events({
  'click .tweets-list-item': function(e, t) {
    e.preventDefault();
    e.stopPropagation();

    Session.set('tweets-list-selected', e.currentTarget.id);
    Overlay.show('Tweet_edit_form');
  }
});

