import { Template } from 'meteor/templating';
import { Tweets } from '../../api/tweets/tweets.js';

import './tweets-list.html';

Template.Tweets_list.onCreated(function() {
  Session.set("tweets_list_mode", false);
  Session.set("tweets_list_filter", true);
});

Template.Tweets_list.onRendered(function() {
  let self = this;
  self.autorun(function(){
    self.subscribe("Tweets.filtered", self.data._id, Session.get("tweets_list_mode"),  Session.get("tweets_list_filter"));
  });
//  this.subscribe("Tweets", this.data._id);
});

Template.Tweets_list.helpers({
  count: function() {
    let c = false;
    if (Tweets.find().count() > 0) {
      c = true;
    }
    return c;
  },
  enabled: function() {
    return Session.get("tweets_list_mode");
  },
  last10: function() {
    return Session.get("tweets_list_filter");
  },
  tweets: function() {
    return Tweets.findFromPublication("Tweets.filtered", {}, { sort: { date: -1 } });
  }
});

Template.Tweets_list.events({
  'change [id=tweets-list-mode]': function(e, t) {
    e.stopPropagation();
    e.preventDefault();
    
    let mode = t.$('select[id="tweets-list-mode"]').val();
    if (mode == 1) {
      Session.set("tweets_list_mode", false);
    } else {
      Session.set("tweets_list_mode", true);
    }
  },
  'change [id=tweets-list-filter]': function(e, t) {
    e.stopPropagation();
    e.preventDefault();

    let mode = t.$('select[id="tweets-list-filter"]').val();
    if (mode == 1) {
      Session.set("tweets_list_filter", true);
    } else {
      Session.set("tweets_list_filter", false);
    }
  },
});

