import { Template } from 'meteor/templating';

import './front.html';

Template.App_front.events({
  'click [data-event-action=front-start]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.root");
  },
});

