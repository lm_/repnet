import { Blaze } from 'meteor/blaze';
import { Template } from 'meteor/templating';
import { Brands } from '../../api/brands/brands.js';

import './brand-page.html';

Template.Brand_page.onCreated(function() {
  let brandId = FlowRouter.getParam('_id');
  this.subscribe("Brand", brandId);
});

Template.Brand_page.helpers({
  brand: function() {
    return Brands.findOne();
  },
  updatedAt: function() {
    return this.updatedAt.toLocaleDateString('ru', { timeZone: 'Europe/Moscow', weekday: 'long', day: 'numeric', month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric' });
  }
});

Template.Brand_page.events({
  'click [data-event-action=brand-back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go('App.private.root');
  },
  'click [data-event-action=brand-update]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Meteor.call("brands/update", this._id);
  },
});

