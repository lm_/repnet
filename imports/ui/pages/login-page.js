import { Template } from 'meteor/templating';

import './login-page.html';

Template.Login_page.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let user = template.$('input[id="login-username"]').val();
    let password = template.$('input[id="login-password"]').val();
    if (user&&password) {
      template.$('button[id="login-button"]').css('pointer-events', 'none');
      Meteor.loginWithPassword(user, password, function(error, result) {
        if (error) {
          sAlert.error(error);
          template.$('button[id="login-button"]').css('pointer-events', 'auto');
          return;
        }
        template.$('button[id="login-button"]').css('pointer-events', 'auto');
        FlowRouter.go(Session.get("redirectAfterLogin")||"App.private.root");
      });
    } else {
      sAlert.error(TAPi18n.__('accounts.alerts.credentials'));
    }
  },
  'click [data-event-action=login-register]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.public.register");
  },
});

