import { Template } from 'meteor/templating';

import './register-page.html';

Template.Register_page.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let user = template.$('input[id="register-username"]').val();
    let password = template.$('input[id="register-password"]').val();
    let email = template.$('input[id="register-email"]').val();
    if (user&&password&&email) {
      template.$('button[id="register-button"]').css('pointer-events', 'none');
      Accounts.createUser({ username: user, password: password, email: email }, function(error, result) {
        if (error) {
          sAlert.error(error);
          template.$('button[id="register-button"]').css('pointer-events', 'auto');
          return;
        }
        template.$('button[id="register-button"]').css('pointer-events', 'auto');
        FlowRouter.go(Session.get("redirectAfterLogin")||"App.private.root");
      });
    } else {
      sAlert.error(TAPi18n.__('accounts.alerts.credentials'));
    }
  },
});

