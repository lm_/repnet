import { Blaze } from 'meteor/blaze';
import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';

import './test-page.html';

var view;

Template.Test_page.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let text = template.$('textarea[id="test-text"]').val();
    if (text) {
      if (view) {
        Blaze.remove(view);
      }
      Meteor.call("synaptic/test", text, function(err, res) {
        if (err) {
          sAlert.error(TAPi18n.__('test.alerts.error', err.reason));
        }
        if (res) {
          sAlert.success(TAPi18n.__('test.alerts.success', res.toString()));
          let data = [];
          data[0] = (res[0]*100).toFixed();
          data[1] = 100 - data[0];
          view = Blaze.renderWithData(Template.Chart_pie, data, $("#test-pie")[0]);
        }
      });
    } else {
      sAlert.error(TAPi18n.__('test.alerts.no-input'));
    }
  }
});

